#!/usr/bin/env perl

# Utility to help find backup files, their metadata, and do something
# with them.  Currently will list the matching files in the snapshots,
# allow you to copy the file to the same path as the queried file with
# the last-modified timestamp attached to the filename, cat the file,
# page a file, or do a diff.  More operations TBD as they make sense to
# add.

# Idea:  Maybe grep across files?  I dunno how useful that would be.

use strict;
use warnings;

# Trap signals, more/less/etc send Broken Pipe sometimes
use sigtrap qw/handler signal_handler normal-signals/;

# Find file paths
use Cwd qw/realpath/;

# Time functions
use POSIX qw/strftime/;

# md5sum for files
use Digest::MD5;

# dirname for finding paths
use File::Basename;

# Use ansi color if available
my $ansi = eval {
    require Term::ANSIColor;
    Term::ANSIColor->import( ':constants', 'colorstrip' );
    1;
};

# To use a local module
use FindBin;
use lib $FindBin::RealBin;

# Load our module
use lssnapshot;

# Get the default path out of our module
my $snapshotPath = $lssnapshot::snapshotPath;

# Are we using BTRFS snapshots?
my $btrfsSnapshot = $lssnapshot::btrfsSnapshot;

# What file size limit to do switch to using quickMD5
my $quickSize = $lssnapshot::quickSize;

# Options for your pager program
my $pagerOpts = $lssnapshot::pagerOpts;

# Options for diff
my $diffOpts = $lssnapshot::diffOpts;

# Disable ANSI if preferred
my $useAnsi = $lssnapshot::useAnsi;

# md5sum string maximum length
my $md5Len = $lssnapshot::md5Len;

# Hint on what to do in case the snapshots don't look correct
our $hint = $lssnapshot::hint;

# Disable ansi if it's disabled in the package
if ( !$useAnsi ) {
    $ansi = '0';
}

# Remove any trailing slash
if ( $snapshotPath =~ /(.*)\/$/x ) {
    $snapshotPath = $1;
}

# Get the number of arguments we passed in to use later
my $args = @ARGV;

# Shortcut to fail if we have no arguments
die 'No arguments passed, usage: ' . $0 . ' file [snapshotPath] [btrfs]'
    if $args == 0;

our $VERSION = '0.1';

# Hash to track found snapshot file metadata
my %files;

# File reference hash to match lines to mtime key of %files hash
my %fileref;

# Get the filename from the first argument
my $fileName = $ARGV[0];

# Exit if we have a path, not a file
if ( -d $fileName ) {
    die 'Filename ' . $fileName . ' appears to be a path; exiting.';
}

# If the file doesn't exist, require absolute path
# Paths such as ~/file will still work fine
if ( !-e $fileName ) {

    # Make sure this is an absolute path if it does not exist
    if ( $fileName !~ m/\/.*/x ) {
        die 'Filename '
            . $fileName
            . ' does not exist; use absolute path to find file in snapshots.'
            . "\n";
    }
    else {
        print 'Warning; filename ' . $fileName . ' does not exist' . "\n";
    }
}

# If there is a second argument assign it to snapshotPath
if ( defined( $ARGV[1] ) ) {
    $snapshotPath = $ARGV[1];
}

# If btrfs is defined make an attempt to find the snapshot path
# from the filename because the snapshot could be at any level
# However, allow the user to override the snapshot path if they need to
if ( $btrfsSnapshot == '1' && ! defined($ARGV[1]) )
{
    my $snapTestPath = dirname($fileName) . '/.snapshots';
    # Loop until we wind up at /.snapshots, if we have that path and
    # we didn't already exit the loop below we have run out of parent
    # directories and it's time to exit the loop
    while ( $snapTestPath ne '/.snapshots' )
    {
        if ( -d $snapTestPath)
        {
            $snapshotPath = $snapTestPath;
            last;
        }
        else
        {
            my $parentTestPath = dirname($snapTestPath);
            $snapTestPath = dirname($parentTestPath) . '/.snapshots';
            # Clean up any double-slashes
            $snapTestPath =~ s/\/\//\//gx;
        }
    }

    # Final check; did we really find a path or just run out of paths
    # to test
    if ( -d $snapTestPath )
    {
        $snapshotPath = $snapTestPath;
    }
    else
    {
        print "Did not find a snapshot path for $fileName\n";
        exit;
    }
}

# User has specified the snapshot path, but is using btrfs
if ( $btrfsSnapshot == '1' && defined($ARGV[1]) )
{
    # If we didn't get a full snapshots path, append /.snapshots
    unless ( $snapshotPath =~ /\.snapshots/ )
    {
        $snapshotPath .= '/.snapshots';
    }
}

# Make sure the snapshot file path exists
if ( ! -d $snapshotPath ) {
    die 'Snapshot path '
        . $snapshotPath
        . ' does not exist or is invalid' . "\n\n"
        . $hint;
}

# Make sure we can read from the snapshot path
if ( ! -r $snapshotPath )
{
    die 'Can not read from '
    . $snapshotPath . "\n\n"
    . $hint;
}

# Set up colors and other ANSI codes
# Use color
my $coloroutput = '1';

# If you use a light background terminal set this to 1
my $lightterm = '0';
my ( $r, $c_r, $c_g, $c_c, $c_m, $c_b, $c_y, $c_w, $cl,
    $clearrestscreen, $clearscreen, $topleft, $bold );

# Populate our color variables as defined in coloroutput and lightterm
colorTags();

# Track our original file md5
my $sourcemd5 = '';

# Get the real, absolute path of this file.  Non-existant files are still okay.
my $absPath = realpath($fileName);

# Path that we want to look for these files under
my $glob = "$snapshotPath/*/$absPath";

# Modify the glob if using btrfs, the path is a little different than
# just a prefix on the path, such as /home/test would be
# /home/.snapshots/2/snapshot/test

# First thing clean up any doubled slashes
$snapshotPath =~ s/\/\//\//gx;

if ( $btrfsSnapshot )
{
    my ($snapPath, undef) = split(/.snapshots/, $snapshotPath);
    # If the path isn't / we have to do some splitting to generate
    # the glob
    if ( $snapPath ne '/' )
    {
        my (undef, $postPath) = split(/$snapPath/, $absPath);
        if ( ! defined($postPath) )
        {
            die 'Could not find snapshot path of ' .$absPath . ' using ' . $snapshotPath . "\n";
        }
        $glob = "$snapshotPath/*/snapshot/$postPath";
    }
    # Otherwise just put it together
    else
    {
        $glob = "/.snapshots/*/snapshot" . "$absPath";
    }
}

# Escape brackets
$glob =~ s/\[/\\[/gx;
$glob =~ s/\]/\\]/gx;

# Get all files that match this glob
my @files = glob($glob);

my $fileCount = @files;

if ( $fileCount > 0 ) {

    # Get the md5sum of the original file if it exists
    if ( -e $absPath ) {
        my ($dev,   $ino,     $mode, $nlink, $uid,
            $gid,   $rdev,    $size, $atime, $mtime,
            $ctime, $blksize, $blocks
        ) = stat($absPath);
        open( my $fh, "<", "$absPath" ) or die 'Can not open ' . $absPath;
        binmode($fh);
        if ( $size < $quickSize )
        {
            $sourcemd5 = Digest::MD5->new->addfile($fh)->hexdigest;
        }
        else
        {
            $sourcemd5 = quickMD5($fh);
        }
        close($fh);
    }

    # Iterate over our list of files and gather metadata on them
    foreach my $file (@files) {
        my ($dev,   $ino,     $mode, $nlink, $uid,
            $gid,   $rdev,    $size, $atime, $mtime,
            $ctime, $blksize, $blocks
        ) = stat($file);
        my $mtimeStr = strftime( "%m/%d/%Y %H:%M:%S", localtime $mtime );

        # Get the md5sum of the file
        open( my $fh, "<", "$file" ) or die 'Can not open ' . $file;
        binmode($fh);
        my $md5;
        if ( $size < $quickSize )
        {
            $md5 = Digest::MD5->new->addfile($fh)->hexdigest;
        }
        else
        {
            $md5 = quickMD5($fh);
        }
        close($fh);
        

        # Create our hash data
        if ( !exists( $files{$mtime} ) ) {
            # Make sure this file is unique by its md5sum
            my $unique = '1';
            foreach my $key (keys %files)
            {
                if ( $files{$key}{'md5'} eq $md5 )
                {
                    $unique = '0';
                }
            }

            # If we didn't find it in the hash, add it
            if ( $unique )
            { 
                $files{$mtime}{'mtimeStr'} = $mtimeStr;
                $files{$mtime}{'md5'}      = $md5;
                $files{$mtime}{'file'}     = $file;
                $files{$mtime}{'size'}     = $size;

                # Get the snapshot name out of the path
                if ( $file =~ m/$snapshotPath\/(.*)\//x ) {
                    my ( $snap, undef ) = split( '/', $1 );
                    $files{$mtime}{'snap'} = $snap;
                }
            }
        }
    }

    # Print our list of file metadata
    printFiles();

    # Loop prompt until exit
    while (1) {

        # Supress warnings in case we don't use colors
        no warnings 'uninitialized';
        print "\n"
            . 'Choose operation '
            . $c_c . 'd'
            . $r . '/'
            . $c_c . 'c'
            . $r . '/'
            . $c_c . 'p'
            . $r . '/'
            . $c_c . 'm'
            . $r . '/'
            . $c_c . 'l'
            . $r . '/'
            . $c_c . 'll'
            . $r . '/'
            . $c_c . 'q'
            . $r . '/'
            . $c_c . '?'
            . $r . ' ['
            . $c_b
            . 'number'
            . $r . ']'
            . $r . ' ('
            . $c_b
            . 'number'
            . $r . '): ';

        # Re-enable warnings
        use warnings 'uninitialized';

        my $input = <STDIN>;

        # Just print the prompt again on invalid input
        next unless $input;

        chomp $input;

        my ( $op, $line, $linetwo ) = split( / /, $input );

        if ( !defined($op) ) { next; }

        # Quit
        if ( $op eq 'q' ) {
            last;
        }

        # List files
        if ( $op eq 'l' ) {
            printFiles();
            next;
        }

        # Long list files
        if ( $op eq 'll' ) {
            printFiles("long");
            next;
        }

        # Help
        if ( $op eq '?' ) {
            print <<'EOH';

  d 1 - Diff file line 1 against source file
d 3 4 - Diff file line 3 against file 4
  c 2 - Copy file line 2 to same path as source file
  p 3 - Print the contents of file 3
  m 4 - Page the contents of file 4
    l - List files again
   ll - Long list files
    q - Quit
EOH

            next;
        }

        # Make sure we got a valid line number to work against
        if ( defined($line) && $line =~ /[0-9]+/x ) {
            if ( exists( $fileref{$line} ) ) {

                # Get the mtime value out of the fileref so we can reference
                # it in the %files hash
                my $mtime = $fileref{$line}{'mtime'};

                # We use this in most operations, pull it out as its own var
                my $snapshotFile = $files{$mtime}{'file'};

                # Copy
                if ( $op eq 'c' ) {
                    my $dateStr = $files{$mtime}{'mtimeStr'};

                   # Replace slashes and spaces from the path with underscores
                    $dateStr =~ s/[\/\ ]/_/gx;
                    my $dest = $absPath . '-' . $dateStr;
                    system( "cp", "$snapshotFile", "$dest" );
                    print 'Copied '
                        . $c_c
                        . $snapshotFile
                        . $r . ' to '
                        . $c_c
                        . $dest
                        . $r;
                    next;
                }

                # Diff
                if ( $op eq 'd' ) {
                    my $var = '';
                    my @opts;

                # If we specified a second file, compare the two to each other
                    if ( defined($linetwo) && exists( $fileref{$linetwo} ) ) {
                        my $mtimetwo        = $fileref{$linetwo}{'mtime'};
                        my $snapshotFileTwo = $files{$mtimetwo}{'file'};
                        print "\nDiffing "
                            . $snapshotFile . ' and '
                            . $snapshotFileTwo . "\n";
                        push( @opts, $diffOpts ) if $diffOpts;
                        push( @opts, $snapshotFileTwo );
                        push( @opts, $snapshotFile );
                        $var = system( "diff", @opts );
                    }
                    else {
                        push( @opts, $diffOpts ) if $diffOpts;
                        push( @opts, $snapshotFile );
                        push( @opts, $absPath );
                        $var = system( "diff", @opts );
                    }
                    if ( $var ne '' ) {
                        print "$var";
                    }
                    else {
                        print "\nThe two files are the same\n";
                    }
                    next;
                }

                # Print
                if ( $op eq 'p' ) {
                    system( "/bin/cat", "$snapshotFile" );
                    next;
                }

                # Pager
                if ( $op eq 'm' ) {
                    my $pagerbin = $ENV{PAGER} || 'less';
                    my @opts;
                    push( @opts, $pagerOpts ) if $pagerOpts;
                    push( @opts, $snapshotFile );
                    system( $pagerbin, @opts );
                    next;
                }

                # If we get here, nothing matched
                print 'Invalid operation ' . $op . "\n";
            }
            else {
                print 'Invalid file ' . $line . "\n";
            }
        }
        else {
            print 'Invalid op, line number or no line number selected' . "\n";
        }
    }
}
else {
    print 'No copies of '
        . $absPath
        . ' in snapshot path '
        . $snapshotPath . "\n";
}

exit;

# Chunked md5, rather than whole file.  https://www.perlmonks.org/?node_id=991162
# 2.5x faster give or take
sub quickMD5 {
    my $fh = shift;
    my $md5 = new Digest::MD5->new;

    $md5->add( -s $fh );

    my $pos = 0;
    until( eof $fh ) {
        seek $fh, $pos, 0;
        read( $fh, my $block, 4096 ) or last;
        $md5->add( $block );
        $pos += 1024**2;
    }

    # Prepend a & to indicate this waas a quickMD5 checksum visually
    my $str = '&' . $md5->hexdigest;
    return $str;
}

# Print the known files
sub printFiles {
    my ($opt) = @_;

    # Simple counter to create ids so they order with our sort
    my $fileid = '1';

    print "\n" . 'Available backups of ' . $absPath . "\n\n";

    foreach my $mtime ( sort { $b <=> $a } keys %files ) {
        my $md5match = "";

        my $md5      = $files{$mtime}{'md5'};
        my $mtimeStr = $files{$mtime}{'mtimeStr'};
        my $file     = $files{$mtime}{'file'};
        my $snap     = $files{$mtime}{'snap'};
        my $size     = fileSize( $files{$mtime}{'size'} );

        if ( $md5 eq $sourcemd5 ) {
            $md5match = "*";

            # No need to continue if we only have one file and it is the
            # same as the file we're comparing against
            my $filecount = keys %files;
            die 'Exiting; no unique files found in snapshot for '
                . $fileName
                . ' (found ' . $fileCount . ' copies total)' 
                . "\n" if ( $filecount == '1' );
        }

        # For color tags and md5 tags, avoid warnings here
        no warnings 'uninitialized';

        # If long format, print the full path first
        if ( defined($opt) && $opt eq "long" ) {
            printf( "%2d: %s%s%s\n", $fileid, $c_m, $file, $r );
        }

        # Otherwise, just print the number
        else {
            printf( "%2d: ", $fileid );
        }

        # Print the metadata for this file
        printf(
            "%s%s%s (%s%9s%s) [%s%9s%s] [%s%.*s%s] %s%s%s%s\n",
            $c_b, $mtimeStr, $r,   $c_m,      $snap,   $r,
            $c_c, $size,     $r,   $c_g,      $md5Len, $md5,
            $r,   $bold,     $c_r, $md5match, $r
        );

        use warnings 'uninitialized';

        # Update our fileref hash
        $fileref{$fileid}{'mtime'} = $mtime;
        $fileid++;
    }

    return;
}

# Pretty up our file size
sub fileSize {
    my ($size) = @_;
    my $sizeunit = 'b';
    if ( $size > 1024.0 ) {
        $sizeunit = 'Kb';
        $size /= 1024.0;
    }
    if ( $size > 1024.0 ) {
        $sizeunit = 'Mb';
        $size /= 1024.0;
    }
    if ( $size > 1024.0 ) {
        $sizeunit = 'Gb';
        $size /= 1024.0;
    }

    my $finalsize = sprintf( "%.1f %s", $size, $sizeunit );
    return $finalsize;
}

# Set up our color tags
sub colorTags {

    # If we're colorizing, set up our defaults
    if ( $coloroutput && $ansi ) {
        $r = RESET();

        $bold = BOLD();

        # Don't use bright colors on a light terminal theme
        if ($lightterm) {
            $c_r = RED();
            $c_g = GREEN();
            $c_c = CYAN();
            $c_m = MAGENTA();
            $c_b = BLUE();
            $c_y = YELLOW();
            $c_w = WHITE();
        }
        else {
            $c_r = BRIGHT_RED();
            $c_g = BRIGHT_GREEN();
            $c_c = BRIGHT_CYAN();
            $c_m = BRIGHT_MAGENTA();
            $c_b = BRIGHT_BLUE();
            $c_y = BRIGHT_YELLOW();
            $c_w = BRIGHT_WHITE();
        }
    }

    # Go to start of line, clear entire line
    $cl = "\r\033[2K";

    # Clear from cursor to the end of the screen
    $clearrestscreen = "\033[J\r";

    # Clear the entire screen
    $clearscreen = "\033[2J";

    # Move cursor to the 0,0 position
    $topleft = "\033[0;0H";

    return;
}

# Handle signals
sub signal_handler {
    my ($signal) = @_;

    # Ignore PIPE
    if ( $signal ne "PIPE" ) {
        die "\n" . 'Caught signal ' . $signal;
    }

    return;
}
