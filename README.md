# lssnapshot

Have you ever had the need to look at the changes you've made to a file over time?  If you have any backup program that makes predictable paths, or use btrfs snapshots?  lssnapshot can help.  It can search through backup snapshots and find unique versions of files and lists them in newest to oldest including the snapshot name, it can even list the full path of the filename for copy/paste if you need to use the filename outside of lssnapshot.

However you may not need to do that, as lssnapshot can diff, cat, page, or copy one of the copies you select out to the location of the current file with a timestamp attached to it.  The original file is never modified.

# Requirements

Digest::MD5 is required in order to find unique files.

Term::ANSIColor is an optional requirement to colorize the output to make things a bit easier to view.  If you wish to disable ANSI output you can do that in the module.

File::Basename is required for pruning directory names to find parents.

You may also need to install sigtrap, which may be a separate package but is base Perl.

# Module Setup

If using btrfs, set btrfsSnapshot to 1.  The script will look for a .snapshots path in the parent directory and if it doens't find one it'll keep going up the tree until it hits the root directory and fails if it does not find one.  

Unless you're using btrfs, you must modify lssnapshot.pm to point to the path of your snapshots.  Your snapshots must be in the form of some path followed by your filesystem paths, such as /mnt/snapshots/snapshotnames/etc/fstab as an example full path to a copy of /etc/fstab.  snapshotnames here refers to the unique tag or name of the snapshot will will be shown in lssnapshots and is generally going to be a date/timestamp of some sort.  The format does not matter.  I've used this on home-grown snapshots as well as restic backup snapshots.

quickSize in the module allows you to determine when we use a chunked MD5 algorithm which calculates the MD5 from 4k of every 1MB of data, effectively making it 2-3x faster in practice.  However you would only want to use this on files that are larger than 1MB.

pagerOpts let you adjust the options to pass to your pager.  The pager used is $ENV{PAGER} if defined, or defaults to less if not.  

diffOpts similarly allows you to set options to use with diff.

You can also modify the $hint in the module to give yourself a hint on how to make your snapshot accessible, such as needing to mount the drive or restic mount, etc, and this will be shown if the script detects the snapshot path doesn't seem to have the expected things in it.

You can undef $pagerOpts, $diffOpts, or $hint in the module if you do not want to set these options.

You can optionally disable ANSI in the module as well as adjust the length of the md5sum string shown in the list.

# btrfs

The script automatically finds the snapshot directory for a file because it's always in some parent above the file so there is no need to define a snapshot path or ever give it on the command line; the script does it for you.  This is mentioned in the module section, but wanted to mention it in its own section as well.

If you have previously made snapshots in another path, such as / and then added a subvolume for /home and started making snapshots of it, you can still manually specify the snapshot path of / to see copies of the file there.  In general if your snapshot path is not a parent directory of the file you can specify the snapshot path and it will use it.  The script will automatically add '/.snapshots' to the path for you if you did not specify it.

# Usage

Continuing the example of /etc/fstab, to look for all unique copies of this file in your snapshots you would simply do the following:

lssnapshot.pl /etc/fstab [snapshotpath]

The filename to look for is required, the snapshotpath is optional so you can use an alternate snapshot path than the one defined in the module.

It will take a few seconds to find all of the copies, do a checksum, and then will list all of the versions found.  It will also list, in this order:

Number of file in list, timestamp of the file, snapshot name the file was found in, the size of the file, and the checksum of the file, and a * if the checksum matches that of the source file itself.  Here is an example:

    1: 03/08/2022 16:58:59 (2022-03-08T18:01:01-05:00) [ 122.1 Kb] [85c450283b309b7c] *
    2: 03/04/2022 08:42:48 (2022-03-04T18:01:01-05:00) [ 122.1 Kb] [0f74539026778cad] 
    3: 03/03/2022 10:35:44 (2022-03-03T18:01:02-05:00) [ 122.0 Kb] [6d9d8539f073d8b3] 
    4: 02/25/2022 21:30:25 (2022-02-26T18:01:01-05:00) [ 121.8 Kb] [bddd24cdb64521ac] 

You are then prompted for an operation to perform.  If you wish to compare the newest file against the second newest, d 1 2 would do a diff against file 1 and 2 in the list, etc.  The full list of operations can be shown by inputting ? and are as follows:

      d 1 - Diff file line 1 against source file
    d 3 4 - Diff file line 3 against file 4
      c 2 - Copy file line 2 to same path as source file
      p 3 - Print the contents of file 3
      m 4 - Page the contents of file 4
        l - List files again
       ll - Long list files
        q - Quit

The copy operation will copy the file to the same path as the original file but with the timestamp of the file appended to it, so for example if the timestamp of file 1 was 03/10/2022 08:07:22 the copy would be named /etc/fstab-03_10_2022_08:07:22, again using an example of /etc/fstab as our file path we're looking for.

In order to speed up the checksum process, which is by far the task which takes the longest in execution, for files that are larger than the defined quickSize will use the chunked MD5 algorithm mentioned in the Module Setup section.  If quickMD5 is used, the md5sum shown will be prefixed with a & to indicate this.  Note that you can't compare this md5sum against the file manually using md5sum in a terminal for example.

Note that if there are no unique copies of the file, the script will exit and let you know that is the case as there's no valid operations to perform when the file and all snapshot copies are the same.