package lssnapshot;

use Exporter;

our @EXPORT = qw(snapshotPath);

# Are we using btrfs Snapshot, 1 = yes
our $btrfsSnapshot = '0';

# Get the default path out of our module; not needed if using btrfs
our $snapshotPath = "/mnt/restic/snapshots/";

# What file size limit to do switch to using quickMD5 in bytes
# 1M minimum is suggested, using 1.5MB as an example here
our $quickSize = 1048576 * 1.5;

# Options for your pager program, set to undef to not use
our $pagerOpts = '-FXMe';

# Options for diff, set to undef to not use
our $diffOpts = '-NU3';

# Disable ANSI if preferred
our $useAnsi = '1';

# md5sum string maximum length, -1 would be no truncation
our $md5Len = '16';

# Hint on what to do in case the snapshots don't look correct
our $hint = "Are you root?  Is restic mounted?  gcp-restic.sh local mount\n";

1;
